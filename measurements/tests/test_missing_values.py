import json
from unittest import TestCase
import pytest
from missing_values import get_measurements_with_missing_values

class measurements_valuesTestCase(TestCase):
# Test case 1: Empty measurements
    def test_empty_measurements():
        result = get_measurements_with_missing_values([], "value", 0, 10)
        assert result == []

    # Test case 2: Basic case with valid measurements
    def test_basic_valid_measurements():
        measurements = [
            {"collection_date": "2023-01-01 12:00:00", "value": 10},
            {"collection_date": "2023-01-01 12:10:00", "value": 20},
            {"collection_date": "2023-01-01 12:20:00", "value": 30},
        ]
        result = get_measurements_with_missing_values(measurements, "value", 0, 10)
        expected_result = [
            ["2023-01-01 12:00:00", 10],
            ["2023-01-01 12:10:00", 20],
            ["2023-01-01 12:20:00", 30],
        ]
        assert result == expected_result

    # Test case 3: Mismatched minute for the original_measurement
    def test_mismatched_minute():
        measurements = [
            {"collection_date": "2023-01-01T00:00:00", "value": 10},
            {"collection_date": "2023-01-01T00:14:00", "value": 14},
        ]
        with pytest.raises(AssertionError):
            result = get_measurements_with_missing_values(measurements, "value", 0, 10)

    # Test case 4: Measurements with missing start and end
    def test_missing_start_and_end():
        measurements = [
            {"collection_date": "2023-01-01 12:20:00", "value": 30},
        ]
        result = get_measurements_with_missing_values(measurements, "value", 0, 10)
        expected_result = [
            ["2023-01-01 12:00:00", 0],
            ["2023-01-01 12:10:00", 0],
            ["2023-01-01 12:20:00", 30],
        ]
        assert result == expected_result
