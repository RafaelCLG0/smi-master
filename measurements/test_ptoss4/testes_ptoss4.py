import json
from unittest import TestCase
import pytest
from utils.validator import ValidationException
from measurements.utils import validate_start_date, validate_end_date


class TestDateValidation(TestCase):
    def test_validate_start_date_with_empty_value(self):
        with self.assertRaises(ValidationException) as context:
            validate_start_date("")

    def test_validate_end_date_with_empty_value():
        with pytest.raises(ValidationException):
            validate_end_date("")

    def test_validate_start_date_with_date_before_2020():
        with pytest.raises(ValidationException):
            validate_start_date("2019-12-31 23:59:59")
    
    def test_validate_end_date_with_date_before_2020():
        with pytest.raises(ValidationException):
            validate_end_date("2019-12-31 23:59:59")
    
    def test_validate_start_date_with_incorrect_date_format():
        with pytest.raises(ValidationException):
            validate_start_date("2023-12-03")
    
    def test_validate_end_date_with_incorrect_date_format():
        with pytest.raises(ValidationException):
            validate_end_date("2023-12-03")