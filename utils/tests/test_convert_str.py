import logging
import pytest
from datetime import datetime
from pathlib import Path
from helpers import string_to_date, parse_date, format_date_range, log_execution_time

@pytest.fixture
class helpersTestCase(TestCase):

    def test_string_to_date_valid_string(self):
        date_str = "2023-11-19T12:30:00Z"
        result = string_to_date(date_str)
        self.assertIsInstance(result, str)

    def test_string_to_date_valid_datetime(self):
        date_obj = datetime.now()
        result = string_to_date(date_obj)
        self.assertEqual(result, date_obj)

    def test_string_to_date_invalid_type(self):
        with self.assertRaises(TypeError):
            string_to_date(123)

